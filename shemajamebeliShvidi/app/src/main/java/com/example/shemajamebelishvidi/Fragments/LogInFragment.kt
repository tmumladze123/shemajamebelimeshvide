package com.example.shemajamebelishvidi.Fragments

import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.withCreated
import androidx.navigation.fragment.findNavController
import com.example.shemajamebelishvidi.Network.retrofitInstance
import com.example.shemajamebelishvidi.R
import com.example.shemajamebelishvidi.databinding.FragmentLoginBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class LogInFragment() : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding :: inflate){
    override fun start() {
    watchers()
    listeners()
    }
    fun watchers()
    {
       // binding.btnLogin(watchers())
    }
    fun listeners()
    {
        binding.btnLogin.setOnClickListener {
            if(binding.username.text.toString().isNotEmpty()
                && binding.username.text.toString().isNotEmpty())
            {
                login(binding.username.text.toString(),binding.username.text.toString())
            }
        }
        binding.btnRegister.setOnClickListener {
            findNavController().navigate(R.id.action_logInFragment_to_registerFragment)
        }
    }

    fun login(email:String, password:String)
    {
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(Dispatchers.IO){
                var result = retrofitInstance.userApi.logInUser(email, password)
                var token=result.body()!!.token
            }
        }
    }

}