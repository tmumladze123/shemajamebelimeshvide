package com.example.shemajamebelishvidi.Network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object retrofitInstance {

    private val retrofit by lazy {
        Retrofit.Builder().baseUrl("https://reqres.in/api/").addConverterFactory(
            GsonConverterFactory.create())
            .build()
    }
    val userApi: Api by lazy {
        retrofit.create(Api::class.java)
    }

}