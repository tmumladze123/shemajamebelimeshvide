package com.example.shemajamebelishvidi.Fragments

import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.shemajamebelishvidi.Network.retrofitInstance
import com.example.shemajamebelishvidi.databinding.FragmentRegisterBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class registerFragment() : BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {
    override fun start() {
        listener()
    }

    fun listener() {
        binding.btnRegister.setOnClickListener {
            if (binding.email.text.toString().isNotEmpty() &&
                binding.etPassword.text.toString().isNotEmpty() &&
                binding.etPassword.text.toString() == binding.repeatPassword.text.toString()
            )
                register(binding.email.text.toString(), binding.etPassword.text.toString())
        }
        binding.btnBack.setOnClickListener { findNavController().popBackStack() }
    }

    fun register(email: String, password: String) {
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                var result = retrofitInstance.userApi.registerUser(email, password)
                var token = result.body()!!.token
                var id = result.body()!!.id
            }
        }
    }
}