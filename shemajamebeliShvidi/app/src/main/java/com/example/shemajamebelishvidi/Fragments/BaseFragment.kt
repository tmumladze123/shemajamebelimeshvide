package com.example.shemajamebelishvidi.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

typealias Inflater <RD> = (LayoutInflater, ViewGroup?, Boolean) -> RD
abstract class BaseFragment <VB : ViewBinding> (var inflate: Inflater<VB>) : Fragment() {

    private var _binding: VB? = null
    open val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding=inflate.invoke(inflater,container,false)
        start()
        return binding.root
    }
    abstract fun start()
    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }

}