package com.example.shemajamebelishvidi.Data


import com.google.gson.annotations.SerializedName

data class login(
    @SerializedName("token")
    val token: String
)