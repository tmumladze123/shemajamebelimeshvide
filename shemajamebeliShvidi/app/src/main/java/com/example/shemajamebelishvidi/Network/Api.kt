package com.example.shemajamebelishvidi.Network

import com.example.shemajamebelishvidi.Data.login
import com.example.shemajamebelishvidi.Data.register
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {
    @POST("register")
    suspend fun registerUser(
        @Query("email") email: String?,
        @Query("password") password: String?
    ) : Response<register>
    @POST("login")
    suspend fun logInUser(
        @Query("email") email: String?,
        @Query("password") password: String?
    ): Response<login>
}