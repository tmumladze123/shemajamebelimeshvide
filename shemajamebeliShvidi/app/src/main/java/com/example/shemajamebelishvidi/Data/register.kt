package com.example.shemajamebelishvidi.Data


import com.google.gson.annotations.SerializedName

data class register(
    @SerializedName("id")
    val id: Int,
    @SerializedName("token")
    val token: String
)